/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author MDay
 */
public class AccessUtil 
{
    public static boolean isAccess (String username, String pageName)
    {
        InputStream inputStream;
        Properties property = new Properties();
 
        try 
        {
            inputStream = AccessUtil.class.getClassLoader().getResourceAsStream("/access.properties");
            property.load(inputStream);
 
            String[] accessList = property.getProperty(username).split(",");
            for (String access : accessList)
            {
                if (pageName.equals("/ServletTask/"+access))
                    return true;
            }
 
        } 
        
        catch (IOException e) 
        {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }
        
        return false;
    }
}
