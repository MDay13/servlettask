/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.File;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author MDay
 */

public class AuthFilter implements Filter
{
    private ServletContext context;
    
    private String login;
    private String password;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException 
    {
        this.context = filterConfig.getServletContext();
        login = filterConfig.getInitParameter("login");
        password = filterConfig.getInitParameter("password");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
    {
        HttpServletRequest  req = (HttpServletRequest) request;
        HttpServletResponse  res = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        
        if (req.getRequestURI().equals("/ServletTask/login") || req.getRequestURI().equals("/ServletTask/"))
            chain.doFilter(request, response);
        else    
        if(session != null  && login.equals(session.getAttribute("name")) 
                            && password.equals(session.getAttribute("password"))  
                            && AccessUtil.isAccess(login, req.getRequestURI()) )
        {
            chain.doFilter(request, response);
        }
        else
        {
            res.sendRedirect("/ServletTask/");
        }
    }

    @Override
    public void destroy() 
    {
        this.context = null;
        login = null;
        password = null;
    }
    
}
